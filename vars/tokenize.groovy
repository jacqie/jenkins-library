def call(String text, Map binding) {
    _tokenize(text, binding)
}

@NonCPS
def _tokenize(String text, Map binding){
    def engine = new org.apache.commons.lang3.text.StrSubstitutor(binding)
    def s = engine.replace(text)
    engine = null
    return s
}

