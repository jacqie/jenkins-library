def call(Map config) {
    withCredentials([azureServicePrincipal("${config.azureServicePrincipal}")]) {
        sh "az login --service-principal -u ${AZURE_CLIENT_ID} -p ${AZURE_CLIENT_SECRET} --tenant ${AZURE_TENANT_ID}"
        sh "az account set --subscription ${AZURE_SUBSCRIPTION_ID} "
        sh "az group create --name deploymentartifacts --location ${config.location}"
        sh "az storage account create --name ${config.deploymentStorageAccount} --resource-group deploymentartifacts --location ${config.location} --sku Standard_LRS --kind StorageV2"
        sh "az storage container create --account-name ${config.deploymentStorageAccount} --name terraformstate"
        accessKey = sh(returnStdout: true, script: "az storage account keys list -n ${config.deploymentStorageAccount} --query \"[0].value\" -o tsv | tr -d '\n'")
        return accessKey
    }   
}