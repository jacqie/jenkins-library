def call() {
    pipeline {
        agent { label "${env.NODE_TO_BUILD}" }

        environment {
            EVENT_TYPE = "${env.EVENT_TYPE}"//getEventType(env.GERRIT_EVENT_TYPE)
            WORKSPACE = pwd()
            FOLDER="${WORKSPACE}/cluster/${env.ENVIRONMENT_TO_BUILD}"
        }

        stages {

            stage('Prepare statefile storage') {
                when {
                    environment name: 'ACTION', value: 'plan'
                }
                steps {
                    script {
                        accessKey = azureCreateStatefileStorage azureServicePrincipal: env.AZURE_SERVICE_PRINCIPAL,
                            deploymentStorageAccount: "${env.CUSTOMER_NAME}artifacts",
                            location: env.LOCATION
                        //echo accessKey
                    }
                }
            }

            stage('Replace tokens for backend vars') {
                when {
                    environment name: 'ACTION', value: 'plan'
                }
                steps {
                    script {
                        text = readFile("${FOLDER}/backend.txt")
                        def binding = [:]
                            binding.StorageAccountName = "${env.CUSTOMER_NAME}artifacts"                         
                            binding.StorageAccessKey = accessKey
                            binding.StateFileName = "${env.CUSTOMER_NAME}${env.TELEPHONY_PLATFORM}${env.ENVIRONMENT_TO_BUILD}.statefile"
                        writeFile(file: "${FOLDER}/backend.tf", text: tokenize(text, binding)
                        )
                        sh "cat ${FOLDER}/backend.tf"  
                    }
                }
            }

            stage('Fetch Terraform vars') {
                when {
                    environment name: 'ACTION', value: 'plan'
                }
                steps {
                    script {
                        withCredentials([file(credentialsId: "${env.CUSTOMER_NAME}-${env.TELEPHONY_PLATFORM}-${env.ENVIRONMENT_TO_BUILD}-secret", variable: 'tfvars')]) {
                            sh "cp $tfvars $FOLDER/terraform.tfvars.txt"
                            sh "cat $FOLDER/terraform.tfvars.txt"
                        }
                    }
                }
            }

            stage('Replace tokens for Terraform vars') {
                when {
                    environment name: 'ACTION', value: 'plan'
                }
                steps {
                    script {
                        withCredentials([azureServicePrincipal("${config.azureServicePrincipal}")]) {   
                            text = readFile("${FOLDER}/terraform.tfvars.txt")
                            def binding = [:]
                                binding.SubscriptionId = "${AZURE_SUBSCRIPTION_ID}"                         
                                binding.TenantId = "${AZURE_TENANT_ID}"
                                binding.Secret = "${AZURE_CLIENT_SECRET}"
                                binding.ClientId = "${AZURE_CLIENT_ID}"
                        i    writeFile(file: "${FOLDER}/terraform.tfvars.json", text: tokenize(text, binding))
                         }
                        sh "cat $FOLDER/terraform.tfvars.json"
                    }
                }
            }

            stage('Terraform plan') {
                when {
                    anyOf {
                        environment name: 'ACTION', value: 'plan'
                        environment name: 'ACTION', value: 'apply'
                        environment name: 'ACTION', value: 'destroy'
                    }
                }
                steps {
                    sh "cd ${FOLDER} && terraform init"
                    sh "cd ${FOLDER} && terraform plan -out terraform_plan -input=false --var-file ${FOLDER}/terraform.tfvars.json"
                }
            }

            stage('Terraform apply') {
                when {
                    allOf {
                       environment name: 'ACTION', value: 'apply'
                    }
                }
                steps {
                    sh "cd ${FOLDER} && terraform apply -input=false terraform_plan"
                }
            }

            stage('Invoke configuration pipeline') {
                when {
                    allOf {
                        expression {
                            EVENT_TYPE == 'change-merged' || EVENT_TYPE == 'release'
                        }
                    environment name: 'ACTION', value: 'apply'
                    }
                }
                steps {
                    echo "invoking configuration pipeline here"
                }
            }

            stage('Terraform destroy') {
                when {
                    environment name: 'ACTION', value: 'destroy'
                }
                steps {
                    sh "cd ${FOLDER} && terraform destroy --auto-approve --var-file terraform.tfvars.json"
                }
            }

            stage('Terraform clean') {
                when {
                    anyOf {
                        environment name: 'ACTION', value: 'clean'
                    }
                }
                steps {
                    sh "cd ${FOLDER} && rm -rf .terraform"
                }
            }
        }
    }
}
