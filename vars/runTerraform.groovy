/**
 * Executes pipeline for executing Terraform commands against Azure
 *
 * @param config a map of named configuration values:
 * foldername
 *
 * Expects the following environment variables:
 * NODE_TO_BUILD
 * GERRIT_EVENT_TYPE
 * WORKDIR
 * ACTION
 *
 * Expects the following credentials:
 * Azure storage access key to save the Terraform state file
 * Azure principal with permissions on subscription level

 * mail_recipients
 */
def call(Map config) {
    pipeline {
        agent { label "${env.NODE_TO_BUILD}" }

        environment {
            //EVENT_TYPE = getEventType("${env.GERRIT_EVENT_TYPE}")
            EVENT_TYPE = "${env.GERRIT_EVENT_TYPE}"
            WORK_DIR = "./${WORKDIR}"
        }

        stages {

            stage('Prepare statefile') {
                steps {
                    script {
                        //tenant storage account: one storage account per tenant for the statefile
                    def file = new File('terraform.tf')
                     def newConfig = file.text.replace('##ACCESS_KEY##', '${key}')
                     def newConfig = file.text.replace('##ACCESS_KEY##', '${key}')
                     def newConfig = file.text.replace('##ACCESS_KEY##', '${key}')
                     
                        file.text = newConfig
                        sh "sed -i -e 's|##access_key##|${ACCESS_KEY}|g' ${WORK_DIR}/terraform.tf" //injecting storage access key for terraform state
                        sh "sed -i -e 's|##terraform_tfstate_file##|${vars['terraform_tfstate_file']}|g' ${WORK_DIR}/terraform.tf" //injecting terraform statefile
                        }                           
                    }
                }
            }

            stage('Prepare') {
                steps {
                    script {
                        withCredentials([file(credentialsId: env.TFVARS, variable: 'tfvars')]) {
                            sh 'cp $tfvars terraform.tfvars.json'
                        }
                        withCredentials([azureServicePrincipal("${env.AZURE_PRINCIPAL}")]) {
                            vars = readJSON file: 'terraform.tfvars.json'
                            vars['az_subscription_id'] = $AZURE_SUBSCRIPTION_ID
                            vars['az_client_id'] = $AZURE_CLIENT_ID
                            vars['az_secret'] = $AZURE_CLIENT_SECRET
                            vars['az_tenant_id'] = $AZURE_TENANT_ID
                            writeJSON file: 'terraform.tfvars.json', json: vars
                        }                           
                    }
                }
            }

            stage('Replace tokens for statefile') {
                steps {
                    sh "sed -i -e 's|##access_key##|${ACCESS_KEY}|g' ${WORK_DIR}/terraform.tf" //injecting storage access key for terraform state
                    sh "sed -i -e 's|##terraform_tfstate_file##|${vars['terraform_tfstate_file']}|g' ${WORK_DIR}/terraform.tf" //injecting terraform statefile
                }
            }

            stage('Terraform plan') {
                when {
                    anyOf {
                        environment name: 'ACTION', value: 'plan'
                        environment name: 'ACTION', value: 'apply'
                        environment name: 'ACTION', value: 'destroy'
                    }
                }
                steps {
                    sh "cd ${WORK_DIR} && terraform init"
                    sh "cd ${WORK_DIR} && terraform plan -out terraform_plan -input=false --var-file terraform.tfvars.json"
                }
            }

            stage('Terraform apply') {
                when {
                    allOf {
                        expression {
                            EVENT_TYPE == 'change-merged' || EVENT_TYPE == 'release'
                        }
                        environment name: 'ACTION', value: 'apply'
                    }
                }
                steps {
                    sh "cd ${WORK_DIR} && terraform apply -input=false terraform_plan --var-file terraform.tfvars.json"
                }
            }

            stage('Invoke configuration pipeline') {
                when {
                    allOf {
                        expression {
                            EVENT_TYPE == 'change-merged' || EVENT_TYPE == 'release'
                        }
                    environment name: 'ACTION', value: 'apply'
                    }
                }
                steps {
                    echo "invoking configuration pipeline here"
                }
            }

            stage('Terraform destroy') {
                when {
                    allOf {
                        expression {
                            EVENT_TYPE == 'change-merged' || EVENT_TYPE == 'release'
                        }
                    environment name: 'ACTION', value: 'destroy'
                    }
                }
                steps {
                    sh "cd ${WORK_DIR} && terraform destroy --auto-approve --var-file terraform.tfvars.json"
                }
            }
        }

        post('Clean up secrets') {
            always {
                sh "rm -rf terraform.tfvars.json"
            }
        }
    }
}